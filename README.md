# Тестовое задание
## Дано:
Репозиторий с кодом на dotNet core на GitLab.com. Есть несколько тестов.

## Проблема:
1. Код не собирается.
2. Тесты не проходят.
3. Сборка и выкладка пакета не автоматизированы.

## Задачи:    

1. Определить, на каком языке и используя какой фреймворк написан тестовый сервис. (+10% выполнения)
2. Форкнуть проект, починить сборку. (+10% выполнения)
3. Добиться того, что бы проходили тесты (явно где-то в коде есть логическая ошибка и, конечно же, сами тесты изменять нельзя). (+10% выполнения)
4. Попробовать дописать недостающие функции в программе. (+10% выполнения)  
5. Настроить [Continuous Integration (GitLab CI)](https://gitlab.com/help/ci/README.md) со сборкой кода и запуском тестов. (+10% выполнения)
    
    5.1 Альтернатива - написать свой скрипт, который будет выкачивать исходный код и собирать + так же необходим прогон тестов.

6. Добавить в GitLab CI запаковку собранного кода в артефакты. (+10% выполнения)

    5.1 Альтернатива - собранный дистрибутив необходима запаковать (zip, tar.gz, e.t.c) и выложить в общий каталог (файловая шара etc...)

7. Добавить в GitLab CI stage с запаковкой собранного кода в docker image. (+10%)

8. Написать Helm chart, который сможет осуществить деплой в кластер K8s (параметры деплоя - на усмотрение). (+20%)

9. Добавить в GitLab CI шаг деплоя в K8s. (+10%)

## Примечания/советы
* Для работы с кодом на языке C# удобно использовать бесплатные инструменты: [Visual Studio Code](https://code.visualstudio.com) либо [Visual Studio Community](https://visualstudio.microsoft.com/ru/vs/community/).
* Для сборки и запуска тестов удобно использовать готовый image: microsoft/dotnet.
* В качестве раннера предоставляются докер-раннеры на ОС Linux с gitlab.com из коробки.
* Для сборки можно использовать оффициальный контейнер от microsoft указанный выше, а можно и не использовать.
* Весь CI будет выполняться в окружении Linux(debian). Что не мешает туда поставить PowerShell.
* Самый простой способ собрать docker image - описать процесс сборки в Dockerfile и использовать команду docker build.

## Документация
* https://gitlab.com/help/ci/README.md
* https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#define-image-and-services-from-gitlab-ciyml
* https://docs.microsoft.com/ru-ru/dotnet/core/index
* https://xunit.github.io/docs/getting-started-dotnet-core
* https://docs.microsoft.com/ru-ru/dotnet/csharp/getting-started/introduction-to-the-csharp-language-and-the-net-framework
* https://docs.microsoft.com/ru-ru/nuget/reference/nuspec
* https://docs.docker.com/get-started/
* https://helm.sh
* https://docs.docker.com/engine/reference/commandline/build/

## Обратная связь
Если что-то совсем не получается - мы всегда готовы оказать помощь в чате Telegram: https://t.me/joinchat/B7_-z09C-iWgfB7OdMeQGA