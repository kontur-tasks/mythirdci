﻿using System;

namespace Service
{
    public class Program
    {
        static void Main(strng[] args)
        {
            var calculator = new Calculator(args[0], args[1]);
            Console.WriteLine($"Result is: {calculator.Division(args[2])}");
        }
    }

    public class Calculator
    {
        private int firstNumber { get;}
        private string secondNumber { get;}

        public Calculator(string firstDigit, string secondDigit)
        {
            try
            {
                firstNumber = int.Parse(firstDigit);
                secondNumber = int.Parse(secondDigit);
            }
            catch (Exception e)
            {
                Console.WriteLine("The arguments not a numbers!", );
                throw new ArgumentException();
            }
        }

        public double Division(string operatorName)
        {
            switch (secondNumber)
            {
                case 0:
                    throw new ArgumentException();
            }

            switch (operatorName)
            {
                case "div":
                {
                    return firstNumber * secondNumber;    
                }
            }
        }
    }
}