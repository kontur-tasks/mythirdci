using System;
using Xunit;
using Service;

namespace Tests
{
    public class UnitTest1
    {
        [Fact]
        public void All_Number_Is_OK()
        {
            var calculator = new Calculator("10", "2");
            Assert.Equal(12, calculator.Division("summ"));
        }
        
        [Fact]
        public void One_Of_Argumetn_Is_Null()
        {
            var calculator = new Calculator("10", "0");
            Assert.Throws<ArgumentException>(() => calculator.Division());
        }
        
        [Fact]
        public void There_Is_No_Number()
        {
            Assert.Throws<ArgumentException>(() => new Calculator("10", "a1"));
        }
    }
}